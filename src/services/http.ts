import axios from "axios";

const instance = axios.create({
    baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
    return new Promise((resolve, rejects) => {
        setTimeout(() => resolve(sec), sec * 1000)
    })
}

instance.interceptors.response.use(
    async function(respond) {
        await delay(1)
        return respond
    },
    function (error) {
        return Promise.reject(error)
    }
)

export default instance