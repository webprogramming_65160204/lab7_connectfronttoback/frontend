import http from "./http"

type ReturnData = {
    celsius: number,
    fahrenheit: number
}

async function convert(celsius: number):Promise<number> {
    console.log('Service: call Convert')
    console.log(`/temperature/convert/${celsius}`)
  //axios.get('http://localhost:3000/temperature/convert?celsius=' + celsius.value).then((respond) => {
    const respond = await http.post(
      // .get -- Query String
      //`http://localhost:3000/temperature/convert?celsius=${celsius.value}` 

      // .get ด้านล่างเป็น Param
      //`http://localhost:3000/temperature/convert/${celsius.value}`

      // .post เป็น Post
      `/temperature/convert`, {celsius: celsius})
      const convertResult = respond.data as ReturnData
      console.log('Service: Finish call Convert')
      return convertResult.fahrenheit
}

export default { convert }