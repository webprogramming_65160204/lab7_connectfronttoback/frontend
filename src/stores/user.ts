import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  const initialUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    roles: ['user']
  }
  const editedUser = ref<User>(JSON.parse(JSON.stringify(initialUser)))

  // Data Function

  async function getUser(id: number) {
    loadingStore.doLoad()
    const respond = await userService.getUser(id )
    editedUser.value = respond.data
    loadingStore.finish()
  }

  async function getAllUser() {
    loadingStore.doLoad()
    const respond = await userService.getAllUser()
    users.value = respond.data
    loadingStore.finish()
  }

  // ADD and UPDATE
  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if(!user.id) {   // Add new  ถ้าไม่มี id
      console.log('Post' + JSON.stringify(user))
      const respond = await userService.addUser(user)
    } else {           // Update
      console.log('Patch' + JSON.stringify(user))
      const respond = await userService.updateUser(user)
    }

    await getAllUser()
    loadingStore.finish()
  }

  //DELETE 
  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const respond = await userService.delUser(user)

    await getAllUser()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }


  return { users, getAllUser, saveUser, deleteUser, editedUser, getUser, clearForm }
})
